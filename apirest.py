from flask import Flask
from flask import request
from client import get_historical_events, get_response_descriptions
import json

app = Flask(__name__)

@app.route('/')
def hello_world():
    return render_template('index.html')
    

@app.route("/request", methods=["GET"])
def handle_request():
    if request.method == "GET":
        long, lat = request.args.get("long"), request.args.get("lat")
        wdResponse = get_historical_events(lat, long)
        return json.dumps(get_response_descriptions(wdResponse))
