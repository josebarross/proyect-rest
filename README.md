# README #

### Description ###

This is a minimal REST API that gets a latitude and longitud and returns historicals event nearby usin open web data sources.

### Set up server ###

1. `pip install -r requirements.txt`
2. `export FLASK_APP=apirest.py`
3. `flask run`
4. (OPTIONAL) to be in debug mode and reload server on code changes `export FLASK_ENV=development`

### Send request ###

`localhost:5000/request?lat=-70.561532&long=-33.5990054`


